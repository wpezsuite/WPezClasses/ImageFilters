<?php

namespace WPezSuite\WPezClasses\ImageFilters;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

class ClassImageFilters implements InterfaceImageFilters {

	protected $_int_jpeg_quality;
	protected $_bool_remove_w_h;
	protected $_mix_image_classes;


	public function __construct() {

		$this->setPropertyDefaults();

	}

	protected function setPropertyDefaults() {

		$this->_int_jpeg_quality  = false;  // WP default = 82
		$this->_bool_remove_w_h   = false;
		$this->_mix_image_classes = false;
	}


	public function setJpegQuality( $int = false ) {

		if ( ! is_bool( $int ) ) {

			$this->_int_jpeg_quality = (integer) $int;

			return true;

		}

		return false;
	}


	/**
	 * changes the jpg quality of images added via add media
	 *
	 * @param $int_jpeg_quality
	 *
	 * @return mixed
	 */
	public function filterJpegQuality( $int_jpeg_quality ) {

		if ( $this->_int_jpeg_quality !== false ) {
			return $this->_int_jpeg_quality;
		}

		return $int_jpeg_quality;
	}

	public function setRemoveWidthHeight( $bool = true ) {

		if ( is_bool( $bool ) ) {

			$this->_bool_remove_w_h = $bool;

			return true;
		}

		return false;
	}

	/**
	 * removes the width and height from img tags in the content
	 *
	 * @param string $str_html
	 *
	 * @return null|string|string[]
	 */
	public function filterPostThumbnailHTML( $str_html  ) {

		if ( $this->_bool_remove_w_h === true ) {

			$str_html = preg_replace( '/(width|height)="\d*"\s/', "", $str_html );

			return $str_html;
		}

		return $str_html;
	}


	public function setAddClasses( $mix = false ) {

		if ( is_array( $mix ) || is_string( $mix ) ) {
			$this->_mix_image_classes = $mix;

			return true;
		}

		return false;
	}

	/**
	 * add classes to the default image classes
	 *
	 * @param $str_classes
	 *
	 * @return string
	 */
	public function filterGetImageTagClass( $str_classes ) {

		if ( $this->_mix_image_classes !== false ) {

			if ( is_array( $this->_mix_image_classes ) ) {

				$str_add = implode( ' ', $this->_mix_image_classes );
			} else {
				$str_add = $this->_mix_image_classes;
			}

			return $str_classes . ' ' . $str_add;


		}

		return $str_classes;
	}


	// TODO attachment_max_dims < deprecated

	// wp_thumbnail_creation_size_limit - in the codex but not in core code?

	// wp_thumbnail_max_side_length - in the codex but not in core code?


}