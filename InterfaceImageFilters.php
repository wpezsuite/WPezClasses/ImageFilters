<?php

namespace WPezSuite\WPezClasses\ImageFilters;

interface InterfaceImageFilters {

	public function filterJpegQuality($int_jpeg_quality);

	public function filterPostThumbnailHTML( $str_html );

	public function filterGetImageTagClass( $str_classes );

}