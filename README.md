## WPezClasses: Image Filters

__A small collection of WordPress filters related to images; done The ezWay.__

   
> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

_Recommended: Use the WPezClasses autoloader (link below)._

```

use WPezSuite\WPezClasses\ImageFilters\ClassImageFilters as ImgFilters;
use WPezSuite\WPezClasses\ImageFilters\ClassHooks as Hooks;


    
$new_if = new ImgFilters();

$new_if->setJpegQuality(90);
$new_if->setRemoveWidthHeight(true);
$new_if->setAddClasses(['large' => true]);
    
$new_hooks = new Hooks($new_if);
$new_hooks->register();

```

Note: The Content Width setting can (automatically) factor in (or not) into which image sizes you wish to add.


### FAQ

__1) Why?__

Just tryin' to keep it as ez as possible boss. These come up often enough, so making them even ez'ier to use made a lot of sense.

__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 

 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 


### HELPFUL LINKS

 - https://developer.wordpress.org/reference/hooks/jpeg_quality/
 
 - https://wordpress.stackexchange.com/questions/22302/how-do-you-remove-hard-coded-thumbnail-image-dimensions
 
 - https://codex.wordpress.org/Plugin_API/Filter_Reference/get_image_tag_class
 

### TODO

n/a

### CHANGE LOG

- v0.0.2 - Monday 22 Aril 2019
    - UPDATED: interface file / name

- v0.0.1 - Saturday 20 April 2019
  - Hey! Ho!! Let's go!!!